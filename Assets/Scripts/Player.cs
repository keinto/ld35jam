﻿using System.Collections;
using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    float _rotationSpeed = 10;

    [SerializeField]
    public GameObject gameOverMenu;
    public GameObject winMenu;


    bool isDead = false;

    public bool IsDead
    {
        get
        {
            return isDead;
        }
        set
        {
            if (value)
            {
                new StopCameraPulse(0).TriggerEvent();
                new StopCameraRotation(0).TriggerEvent();
                new StopCameraShake(0).TriggerEvent();

                MusicMix.instance.audio.Stop();
            }

            isDead = value;
        }
    }

    public static Player instance;

	[SerializeField]
	private Shape _shape;
	public Shape shape
    { 
		get
        {
            return _shape;
        } 
		set 
		{
			if (value != _shape && (int)value < Enum.GetValues(typeof(Shape)).Length)
            {
				_shape = value;
			}
			else
			{
				_shape = (Shape)0; //always the first
			}
		} 
	}

	[SerializeField]
	public Sprite[] sprites;
	//Sprite newSprite = Resources.Load("Circle", typeof(Sprite)) as Sprite;

	// Use this for initialization
	void Start ()
    {
		_shape = Shape.square;
		GetComponent<SpriteRenderer>().sprite = sprites[(int)_shape];
		instance = this;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (IsDead)
        {
            //TODO: Remove line below after the game is done
            return;
        }
		//the player presses a button change shape A and D
		if (Input.GetKeyUp (KeyCode.Space))
        {
			ChangeShape();

		}

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.RotateAround(new Vector3(0, 0, 0), Vector3.forward, _rotationSpeed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.RotateAround(new Vector3(0, 0, 0), -Vector3.forward, _rotationSpeed * Time.deltaTime);
        }
        
	}

	void ChangeShape()
	{
		int newShape = (int)shape + 1;
		shape = (Shape)newShape;
		GetComponent<SpriteRenderer>().sprite = sprites[(int)shape];
	
	}

}

public enum Shape
{
    square, triangle, circle, hexagon
}
