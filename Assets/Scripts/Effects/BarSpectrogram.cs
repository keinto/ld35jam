using UnityEngine;
using System.Collections.Generic;

public class BarSpectrogram : MonoBehaviour
{
    [SerializeField]
    Sprite sprite;
    [SerializeField]
    Material mat;

    public List<GameObject> objs;

    // Use this for initialization
    void Start()
    {
        objs = new List<GameObject>();

        for (int i = 0; i < 1024; i++)
        {
            GameObject go = new GameObject();
            go.AddComponent<SpriteRenderer>();
            go.GetComponent<SpriteRenderer>().sprite = sprite;
            go.GetComponent<SpriteRenderer>().material = mat;

            objs.Add(go);
            go.transform.parent = this.transform;
            objs[i].transform.position = new Vector3(((-512 + i) / (288.0f / Camera.main.orthographicSize)) + Camera.main.transform.position.x, 0 + Camera.main.transform.position.y, 0);

        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < 1024; i++)
        {

            objs[i].transform.localPosition = new Vector3(((-512 + i) / (288.0f / Camera.main.orthographicSize)) + Camera.main.transform.position.x, 0 + Camera.main.transform.position.y, 0);
            objs[i].transform.localScale = new Vector3(0.05f, 200 * MusicMix.instance.spectrum[i] + 0.1f, 0.1f);
			objs[i].GetComponent<SpriteRenderer>().material.color = new Color(100 * MusicMix.instance.spectrum[i], 1f / (20 * MusicMix.instance.spectrum[i]), 1f / (20 * MusicMix.instance.spectrum[i]), 1);
			if(1024/(MusicMix.instance.audio.clip.length)*(MusicMix.instance.audio.time) <= i)
			{
				objs[i].GetComponent<SpriteRenderer>().material.color = Color.red;
			}
		}

	}
	

}
