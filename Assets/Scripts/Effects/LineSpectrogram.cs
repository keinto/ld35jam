﻿using UnityEngine;
using System.Collections.Generic;

public class LineSpectrogram : MonoBehaviour
{
    [SerializeField]
    bool flip;
    [SerializeField]
    Material mat;
    List<LineRenderer> lines;

	// Use this for initialization
	void Start ()
    {
        lines = new List<LineRenderer>();
	    for (int i = 0; i < 1024; i++)
        {
            GameObject go = new GameObject();
            go.transform.parent = this.transform;
            lines.Add(go.AddComponent<LineRenderer>());
            lines[i].material = mat;
            lines[i].SetWidth(0.5f, 0.5f);
            lines[i].useWorldSpace = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 1; i < 1024; i++)
        {
            /*if (MusicMix.instance.spectrum[i] != 0)
            {
                spec = Mathf.Log(MusicMix.instance.spectrum[i], 2);
                lines[i].SetPositions(new Vector3[] { new Vector3(i - 1, Mathf.Log(MusicMix.instance.spectrum[i - 1], 2)), new Vector3(i, Mathf.Log(MusicMix.instance.spectrum[i], 2)) });

            }*/
/*            lines[i].SetPositions(
                new Vector3[] { new Vector3((-512 + i - 1) / (288.0f / Camera.main.orthographicSize), (MusicMix.instance.spectrum[i - 1] * 250) - Camera.main.orthographicSize, 2),
                new Vector3((-512 + i) / (288.0f / Camera.main.orthographicSize), (MusicMix.instance.spectrum[i] * 250) - Camera.main.orthographicSize, 2) });
            *//*lines[i].transform.position = new Vector3(-512 + i, Mathf.Log(MusicMix.instance.spectrum[i]) + 10, 0);
            lines[i].transform.rotation = Quaternion.Euler(0, 0, Vector3.Angle(new Vector3((-512 + i) / 8.0f, Mathf.Log(MusicMix.instance.spectrum[i]) + 10, 2), new Vector3((-512 + i - 1) / 8.0f, Mathf.Log(MusicMix.instance.spectrum[i - 1]) + 10, 2) * Mathf.Rad2Deg));
            */

            //lines[i].SetPositions(new Vector3[] { new Vector3((-512 + i - 1) / 8.0f, Mathf.Log(MusicMix.instance.spectrum[i - 1]) + 10, 2), new Vector3((-512 + i) / 8.0f, Mathf.Log(MusicMix.instance.spectrum[i]) + 10, 2)});
        }
    }
}
