﻿using UnityEngine;
using System.Collections;
using System;

public class SpawnCircle : Event
{
    float _angle;
    Shape _shape;

    public SpawnCircle(float timer, float angle, Shape shape) : base (timer)
    {
        _angle = angle;
        _shape = shape;
    }
    

    public override void TriggerEvent()
    {
        Spawner.instance.SpawnCircle(_angle, _shape);
    }
}
