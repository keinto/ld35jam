﻿using UnityEngine;
using System.Collections;
using System;

public class StartCameraShake : Event
{

    float _intensity;
    float _shakeAmount;

    public StartCameraShake(float timer, float intensity, float shakeAmount) : base (timer)
    {
        _intensity = intensity;
        _shakeAmount = shakeAmount;
    }


    public override void TriggerEvent()
    {
        Camera.main.GetComponent<CameraShake>().StartShake(_intensity, _shakeAmount);
    }

}
