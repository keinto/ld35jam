﻿using UnityEngine;
using System.Collections;

public class StartCameraPulse : Event {


    float _BPM;
    float _intensity;

    public StartCameraPulse(float timer, float BPM, float intensity) : base (timer)
    {
        _intensity = intensity;
        _BPM = BPM;
    }


    public override void TriggerEvent()
    {
        Camera.main.GetComponent<CameraPulse>().StartPulse(_BPM, _intensity);
    }
}
