﻿using UnityEngine;
using System.Collections;

public class StartCameraRotation : Event {

    float _speed;

    public StartCameraRotation(float timer, float speed) : base (timer)
    {
        _speed = speed;
    }


    public override void TriggerEvent()
    {
        Camera.main.GetComponent<CameraRotation>().StartRotation(_speed);
    }

}
