﻿using UnityEngine;
using System.Collections;

public class StopCameraRotation : Event
{
    public StopCameraRotation(float timer) : base (timer)
    {
    }

    public override void TriggerEvent()
    {
        Camera.main.GetComponent<CameraRotation>().StopRotation();
    }
}
