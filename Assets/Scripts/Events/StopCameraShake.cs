﻿using UnityEngine;
using System.Collections;

public class StopCameraShake : Event {


    float _intensity = 0;
    float _shakeAmount = 0;

    public StopCameraShake(float timer) : base (timer)
    {
    }


    public override void TriggerEvent()
    {
        Camera.main.GetComponent<CameraShake>().StopShake();
    }

}
