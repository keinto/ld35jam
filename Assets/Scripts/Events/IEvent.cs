﻿using UnityEngine;
using System.Collections;

public abstract class Event
{
    private float _timer;

    public float Timer
    {
        get
        {
            return _timer;
        }
        set
        {
            if (value <= 0)
            {
                _timer = 0;
            }
            else
            {
                _timer = value;
            }
        }
    }

    public Event(float timer)
    {
        _timer = timer;
    }

    public abstract void TriggerEvent();
}
