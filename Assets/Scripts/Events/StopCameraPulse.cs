﻿using UnityEngine;
using System.Collections;

public class StopCameraPulse : Event
{


    float _BPM = 0;
    float _intensity = 0;

    public StopCameraPulse(float timer) : base (timer)
    {
    }

    public override void TriggerEvent()
    {
        Camera.main.GetComponent<CameraPulse>().StopPulse();
    }
}
