﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class LevelEventSystem : MonoBehaviour
{
    float delay = 0;
    public static LevelEventSystem Instance;
    Queue<Event>[] levelQueues;
    Queue<Event> currentQueue;
    Event currentEvent;
    int currentLevel;
    float musicLevelTimer;
	bool infinityMode = false;
	int randomNumber = 0;
	static int typesOfShapes = 3;
	private float infiniteTimer = 0;

    Event[] allEvents;

    void Awake()
    {
        Instance = this;
        currentLevel = 3;
        musicLevelTimer = 0;
        levelQueues = new Queue<Event>[] {
            Level1(),
            Level2(),
            Level3(),
			LevelInfinity()
        };

        allEvents = new Event[]{new StartCameraPulse(1F, Random.Range(1F, 10F), Random.Range(1F, 10F)),
            new StartCameraRotation(Random.Range(1F, 10F), Random.Range(1F, 10F)),
            new StartCameraShake(Random.Range(1F, 10F),Random.Range(1F, 10F),Random.Range(1F, 10F)),
            new StopCameraPulse(Random.Range(1F, 10F)),
            new StopCameraRotation(Random.Range(1F, 10F)),
            new StopCameraShake(Random.Range(1F, 10F)),
            new SpawnCircle(Random.Range(1F, 7200F), Random.Range(1F, 10F), (Shape)Random.Range(0, typesOfShapes))};
    }

	// Use this for initialization
	void Start ()
    {
        //currentEvent = currentQueue.Dequeue();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Player.instance.IsDead)
        {
            return;
        }
		if (infinityMode) {
			Debug.Log (currentEvent);
			currentQueue.Enqueue(LevelInfinity().Dequeue());
			currentEvent = currentQueue.Dequeue();

		}

        if (!infinityMode && MusicMix.instance.audio.time >= MusicMix.instance.audio.clip.length - 1)
        {
            Player.instance.winMenu.SetActive(true);
            Player.instance.IsDead = true;
        }

        if (currentEvent != null)
        {

            musicLevelTimer += Time.deltaTime;

            if (musicLevelTimer + delay >= currentEvent.Timer)
            {
                currentEvent.TriggerEvent();
                if (currentQueue.Count > 0)
                {
                    currentEvent = currentQueue.Dequeue();
                }
                else
                {
                    currentEvent = null;
                }
            }
        }
    }

    Event GetRandomEvent()
    {

        Random.seed = (int)Random.Range(0F, 200F);
        randomNumber = Random.Range(0, allEvents.Length - 1);
        return allEvents[randomNumber];
    }

    Queue<Event> Level1()
    {
        Queue<Event> levelEvents = new Queue<Event>();
        for (int i = 0; i < 8; i++)
        {
            levelEvents.Enqueue(new SpawnCircle(2 * i, 0, (Shape)(i % 4)));
        }
        for (int i = 0; i < 8; i++)
        {
            float angle = Random.Range(0.0f, 360.0f);
            levelEvents.Enqueue(new SpawnCircle(16.5f + (2 * i), angle, 0));
            levelEvents.Enqueue(new SpawnCircle(17f + (2 * i), angle + 15, 0));
        }
        for (int i = 0; i < 32; i++)
        {

            levelEvents.Enqueue(new SpawnCircle(32 + (2 * i) + 0.5f, Random.Range(0.0f, 360.0f), (Shape)(i % 4)));
            levelEvents.Enqueue(new StartCameraRotation(32 + (2 * i) + 0.5f, 500));
            levelEvents.Enqueue(new StartCameraRotation(32.05f + (2 * i) + 0.5f, 0));

            levelEvents.Enqueue(new StartCameraRotation(32.25f + (2 * i) + 0.5f, -500));
            levelEvents.Enqueue(new StartCameraRotation(32.3f + (2 * i) + 0.5f, 0));

            levelEvents.Enqueue(new StartCameraRotation(32.5f + (2 * i) + 0.5f, 500));
            levelEvents.Enqueue(new StartCameraRotation(32.55f + (2 * i) + 0.5f, 0));

            levelEvents.Enqueue(new StartCameraRotation(32.75f + (2 * i) + 0.5f, -500));
            levelEvents.Enqueue(new StartCameraRotation(32.8f + (2 * i) + 0.5f, 0));
        }
        levelEvents.Enqueue(new StopCameraRotation(96));
        levelEvents.Enqueue(new StartCameraPulse(96, 10, 10));

        for (int i = 0; i < 15; i++)
        {
            float angle = Random.Range(0.0f, 360.0f);
            int shape = (i % 4);
            levelEvents.Enqueue(new SpawnCircle(98.5f + (2 * i), angle, (Shape)(shape % 4)));
            levelEvents.Enqueue(new SpawnCircle(99f + (2 * i), angle + 15, (Shape)((shape + 1) % 4)));
        }

        levelEvents.Enqueue(new StartCameraShake(116, 12, 0.25f));
        levelEvents.Enqueue(new StartCameraShake(116.5f, 13, 0.25f));
        levelEvents.Enqueue(new StartCameraShake(117, 14, 0.25f));
        levelEvents.Enqueue(new StartCameraShake(117.5f, 14, 0.25f));
        levelEvents.Enqueue(new StartCameraShake(118, 15, 0.5f));
        levelEvents.Enqueue(new StartCameraShake(118.5f, 16, 0.5f));
        levelEvents.Enqueue(new StartCameraShake(119, 17, 0.5f));
        levelEvents.Enqueue(new StartCameraShake(119.5f, 18, 0.5f));
        levelEvents.Enqueue(new StartCameraShake(120, 19, 1));
        levelEvents.Enqueue(new StartCameraShake(120.5f, 20, 1));
        levelEvents.Enqueue(new StartCameraShake(121, 21, 1));
        levelEvents.Enqueue(new StartCameraShake(121.5f, 22, 1));

        levelEvents.Enqueue(new StopCameraShake(132));

        levelEvents = new Queue<Event>(levelEvents.OrderBy(x => x.Timer));

        return levelEvents;
    }

    Queue<Event> Level2()
    {
        //local array event
        Queue<Event> levelEvents = new Queue<Event>();
        //the time is in secs
        for (int i = 0; i < 117; i++)
        {
            levelEvents.Enqueue(new SpawnCircle(i * 2, i * Random.Range(0f, 360f), (Shape)Random.Range(0f, typesOfShapes)));

            if (i >= 16 && i % 2 == 0)
            {
                levelEvents.Enqueue(new StartCameraShake(i, 30, 1));
                levelEvents.Enqueue(new StartCameraShake(i + 0.5f, 0, 0));
                levelEvents.Enqueue(new StartCameraShake(i + 0.7f, 30, 1));
                levelEvents.Enqueue(new StartCameraShake(i + 1.5f, 0, 0));

                levelEvents.Enqueue(new StartCameraPulse(i + 2f, 120, 10));
                levelEvents.Enqueue(new StartCameraPulse(i + 2.5f, 120, 10));
            }
        }
        levelEvents.Enqueue(new StopCameraPulse(119));
        levelEvents.Enqueue(new StopCameraShake(119));

        levelEvents = new Queue<Event>(levelEvents.OrderBy(x => x.Timer));
        return levelEvents;
    }

    Queue<Event> Level3()
    {
        //local array event
        Queue<Event> levelEvents = new Queue<Event>();
        for (int i = 0; i < 118; i++)
        {
            levelEvents.Enqueue(new SpawnCircle(i * 2, i * Random.Range(0f, 360f), (Shape)Random.Range(0f, typesOfShapes)));

            if (i >= 16 && i % 2 == 0)
            {
                levelEvents.Enqueue(new StartCameraShake(i, 30, 1));
                levelEvents.Enqueue(new StartCameraShake(i + 0.5f, 0, 0));
                levelEvents.Enqueue(new StartCameraShake(i + 0.7f, 30, 1));
                levelEvents.Enqueue(new StartCameraShake(i + 1.5f, 0, 0));
            }
        }

        levelEvents.Enqueue(new StopCameraShake(119));
        levelEvents = new Queue<Event>(levelEvents.OrderBy(x => x.Timer));
        return levelEvents;
    }
	Queue<Event> LevelInfinity()
	{

		Queue<Event> levelEvents = new Queue<Event>();
		if (infinityMode == true)
		{
			infiniteTimer += Time.deltaTime;
			//keep making enqeueing random things
			//do it for each drum?
			if (MusicMix.instance.CheckDrum() && infiniteTimer > 1)
			{
				infiniteTimer = 0;
				levelEvents.Enqueue(new SpawnCircle(0, Random.Range(0f, 360f), (Shape)Random.Range(0f, typesOfShapes)));

			}
			levelEvents.Enqueue(GetRandomEvent());
			
		}
		return levelEvents;
	}

    public float GetCheckPoint()
    {
        if (currentLevel == 0) {
			if (musicLevelTimer < 12) {
				return 0;
			} else if (musicLevelTimer < 32) {
				return 12;
			} else if (musicLevelTimer < 64) {
				return 32;
			} else if (musicLevelTimer < 96) {
				return 64;
			} else {
				return 96;
			}
		}
		if (currentLevel == 1) {
			if (musicLevelTimer < 16) {
				return 0;
			} else if (musicLevelTimer < 32) {
				return 12;
			} else if (musicLevelTimer < 64) {
				return 32;
			} else if (musicLevelTimer < 96) {
				return 64;
			} else {
				return 96;
			}
		}
		if (currentLevel == 2) {
			if (musicLevelTimer < 16) {
				return 0;
			} else if (musicLevelTimer < 32) {
				return 12;
			} else if (musicLevelTimer < 64) {
				return 32;
			} else if (musicLevelTimer < 96) {
				return 64;
			} else {
				return 96;
			}
		}
        return 0;
    }

    public void StartLevel()
    {
        StartLevel(currentLevel);
    }

    public void StartLevel(int levelindex)
    {
        StartLevel(levelindex, 0);
    }

    public void StartLevel(float position)
    {
        StartLevel(currentLevel, position);
    }

    public void StartInfiniteMode()
    {
        Player.instance.IsDead = false;
        infinityMode = true;
		currentLevel = 3; //infinity mode
		currentQueue = new Queue<Event>(levelQueues[currentLevel].ToArray());
		
		MusicMix.instance.audio.clip = MusicMix.instance.gameMusic[currentLevel];
		MusicMix.instance.audio.Play();

    }

    public void StartLevel(int levelindex, float position)
    {

        Player.instance.IsDead = false;
        currentLevel = levelindex;
        currentQueue = new Queue<Event>(levelQueues[currentLevel].ToArray());
        MusicMix.instance.audio.clip = MusicMix.instance.gameMusic[currentLevel];
        MusicMix.instance.audio.time = position;
        MusicMix.instance.audio.Play();
        musicLevelTimer = position;
        delay = -0f;
        for (int i = 0; i < currentQueue.Count; i++)
        {
            if (currentQueue.Peek().Timer < musicLevelTimer)
            {
                currentQueue.Dequeue();
                i--;
            }
        }
        currentEvent = currentQueue.Dequeue();

    }


}
