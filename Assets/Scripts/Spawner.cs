﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public static Spawner instance;
    [SerializeField]
    GameObject _circleObject;
    public ObjectPool ObjectPool
    {
        get;
        private set;
    }
    

    float _spawntime = 0;

	// Use this for initialization
	void Start ()
    {
        instance = this;
        ObjectPool = new ObjectPool(_circleObject, this.transform);
	}
	
	// Update is called once per frame
	void Update ()
    {
        /*if (Player.instance.isDead)
            return;

        if (_spawntime <= 0)
        {
            GameObject go = ObjectPool.Spawn(Vector3.zero, Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f)));
            go.transform.localScale = new Vector3(40, 40, 40);
            _spawntime = 2;
        }

        _spawntime -= Time.deltaTime;*/
	}

    public void SpawnCircle(float angle, Shape shape)
    {
        GameObject go = ObjectPool.Spawn(Vector3.zero, Quaternion.Euler(0, 0, angle));
        go.transform.GetChild(0).GetComponent<Passage>().PassageShape = shape;
        go.transform.localScale = new Vector3(40, 40, 40);
    }
}
