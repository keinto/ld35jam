﻿using UnityEngine;
using System.Collections;

public class CircleWall : MonoBehaviour
{
    [SerializeField]
    float _scaleSpeed;

	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Player.instance.IsDead)
            return;

        this.transform.localScale -= (this.transform.localScale * (_scaleSpeed * Time.deltaTime)); 
        if (this.transform.localScale.x <= 0.5)
        {
            this.transform.parent.GetComponent<Spawner>().ObjectPool.Despawn(this.gameObject);
        }
        if(MusicMix.instance.CheckDrum())
        {
            this.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F));
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.GetComponent<Player>() != null)
        {
            col.GetComponent<Player>().IsDead = true;
            Player.instance.gameOverMenu.SetActive(true);
        }
        //TODO: Kill player
    }
}
