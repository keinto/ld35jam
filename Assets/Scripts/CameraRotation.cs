﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour
{
    Quaternion cameraRotation;
    Quaternion _targetRotation;

    public float speed = 0;

    // Use this for initialization
    void Start()
    {
        cameraRotation = Camera.main.transform.rotation;
        cameraRotation = Quaternion.Euler(0, 0, 180);
    }

    // Update is called once per frame
    void Update()
    {
        if (speed != 0)
        {
            Camera.main.transform.Rotate(0, 0, speed * Time.deltaTime);
        }
    }

    IEnumerator ResetCamera()
    {
        while (Quaternion.Angle(Camera.main.transform.rotation, cameraRotation) > 0.1f)
        {
            Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, Quaternion.identity, 2 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        Camera.main.transform.rotation = cameraRotation;
        yield return null;
    }

    public void StartRotation(float speed)
    {
        this.speed = speed;
    }
    public void StopRotation()
    {
        speed = 0;
        StartCoroutine(ResetCamera());
    }
}
