﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour {

	public GameObject canvas;
	private bool _isShowing = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartGame()
	{
		if (_isShowing == true) {
			canvas.SetActive(false);
			Player.instance.IsDead = false;
            LevelEventSystem.Instance.StartLevel(0);
		}

	}
}
