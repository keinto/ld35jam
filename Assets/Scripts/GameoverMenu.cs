﻿using UnityEngine;
using System.Collections;

public class GameoverMenu : MonoBehaviour
{

    public void MainMenu()
    {
        for (int i = 0; i < Spawner.instance.transform.childCount; i++)
        {
            Destroy(Spawner.instance.transform.GetChild(i).gameObject);
        }
        Spawner.instance.ObjectPool.ActiveObject.Clear();

        this.gameObject.SetActive(false);

        MusicMix.instance.audio.clip = MusicMix.instance.menuMusic[0];
        MusicMix.instance.audio.Play();
    }

    public void Continue()
    {
        for (int i = 0; i < Spawner.instance.transform.childCount; i++)
        {
            Destroy(Spawner.instance.transform.GetChild(i).gameObject);
        }
        Spawner.instance.ObjectPool.ActiveObject.Clear();
        LevelEventSystem.Instance.StartLevel(LevelEventSystem.Instance.GetCheckPoint());
        this.gameObject.SetActive(false);
    }
}
