﻿using UnityEngine;
using System.Collections;

public class CameraPulse : MonoBehaviour
{
    float cameraSize;
    public float BPM = 10;
    public float Intensity = 5;

    // Use this for initialization
    void Start ()
    {
        cameraSize = Camera.main.orthographicSize;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (BPM != 0 && Intensity != 0)
        {
            Camera.main.orthographicSize = cameraSize + Mathf.PingPong(Time.time * BPM, Intensity);
        }
    }

    IEnumerator ResetCamera()
    {
        while (!Mathf.Approximately(Camera.main.orthographicSize, cameraSize))
        {
            Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, cameraSize, 2 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        Camera.main.orthographicSize = cameraSize;
        yield return null;
    }

    public void StartPulse(float BPM, float Intensity)
    {
        this.BPM = BPM;
        this.Intensity = Intensity;
    }
    public void StopPulse()
    {
        BPM = 0;
        Intensity = 0;
        StartCoroutine(ResetCamera());
    }
}
