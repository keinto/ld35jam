﻿using System;
using UnityEngine;
using System.Collections;

public class Passage : MonoBehaviour
{
    Shape _passageShape;

    public Shape PassageShape
    {
        get
        {
            return _passageShape;
        }
        set
        {
            _passageShape = value;
            this.GetComponent<SpriteRenderer>().sprite = Player.instance.sprites[(int)value];
        }
    }

    void OnEnable()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Player>() != null)
        {
            if (_passageShape != Player.instance.shape)
            {
                col.GetComponent<Player>().IsDead = true;
                Player.instance.gameOverMenu.SetActive(true);
            }
        }
    }
}
