﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicMix : MonoBehaviour
{
    public AudioClip[] menuMusic;
    public AudioClip[] gameMusic;
    [HideInInspector]
	public AudioSource audio;
    [HideInInspector]
	public float[] spectrum = new float[1024];
    public static MusicMix instance;
	
	void Start()
    {
		audio = GetComponent<AudioSource>();
        instance = this;
	}
	
	void Update()
    {
        float spec = 0;

        audio.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
		int i = 1;
        while (i < spectrum.Length- 224 - 1) {/*
            Debug.DrawLine(new Vector3(i - 1, spectrum[i] + 10, 0), new Vector3(i, spectrum[i + 1] + 10, 0), Color.red);

            Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2), new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);

            Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1), new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);
            Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3), new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.yellow);*/

            List<GameObject> lgo = GameObject.Find("Spawner").GetComponent<Spawner>().ObjectPool.ActiveObject;


            foreach (GameObject go in lgo)
			{
				if(Mathf.Log (spec) > 0.25f)
				{
					go.GetComponent<SpriteRenderer>().color = new Color (Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F));
				}
			}
			//Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1), new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);
			//Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3), new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.yellow);
			i++;
		}
    }

    public bool CheckDrum()
    {
        for (int i = 0; i < 8; i++)
        {
            if (Mathf.Log(spectrum[i]) > -2f)
            {
                return true;
            }
        }
        return false;
    }
}
