﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    Vector3 cameraPosition;
    Vector3 _targetPosition;

    public float intensity = 2;
    public float shakeAmount = 1;


    // Use this for initialization
    void Start ()
    {
        cameraPosition = Camera.main.transform.position;
        _targetPosition = cameraPosition + Random.onUnitSphere * shakeAmount;
    }

    // Update is called once per frame
    void Update()
    {
        if (intensity != 0 && shakeAmount != 0 && Vector3.Distance(Camera.main.transform.position, _targetPosition) <= 0.1f)
        {
            _targetPosition = cameraPosition + Random.onUnitSphere * shakeAmount;
            _targetPosition.z = -10;
        }

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, _targetPosition, intensity * Time.deltaTime);
    }

    IEnumerator ResetCamera()
    {
        while (Vector3.Distance(Camera.main.transform.position, cameraPosition) > 0.1f)
        {
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, cameraPosition, 2 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        Camera.main.transform.position = cameraPosition;
        yield return null;
    }

    public void StartShake(float intensity, float shakeAmount)
    {
        this.shakeAmount = shakeAmount;
        this.intensity = intensity;
    }
    public void StopShake()
    {
        shakeAmount = 0;
        intensity = 0;
        StartCoroutine(ResetCamera());
    }
}
