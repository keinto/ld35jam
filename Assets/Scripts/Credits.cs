﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour
{
    public void OnEnable()
    {
        MusicMix.instance.audio.clip = MusicMix.instance.menuMusic[1];
        MusicMix.instance.audio.Play();
    }

    public void OnDisable()
    {
        MusicMix.instance.audio.clip = MusicMix.instance.menuMusic[0];
        MusicMix.instance.audio.Play();

    }
}
