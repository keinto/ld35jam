﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour
{
	private Color color1;
	private Color color2;
	public float duration = 3.0F; //standard
	private float timer = 0;

	void Start()
    {
		color1 = new Color (Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F));
		color2 = new Color (Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F));
		Camera.main.clearFlags = CameraClearFlags.SolidColor;
	}

	void Update() {
		timer += Time.deltaTime;
		if (timer > duration)
        {
			color1 = color2;
			color2 = new Color (Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F), Random.Range(0.0F, 1.0F));
			timer = 0;
		} 
		Camera.main.backgroundColor = Color.Lerp (color1, color2, timer);
	}
}
